#!/bin/sh

set -e

BACKUP_NAME="$(date +"%s_%Y-%m-%d")_${MYSQL_DATABASE}"
/usr/bin/mysqldump -h${MYSQL_HOST} -u${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} > "${BACKUP_NAME}.sql" && \
gzip "${BACKUP_NAME}.sql" && \
curl -T "${BACKUP_NAME}.sql.gz" -u ${FTP_USER}:${FTP_PASSWORD} ${FTP_URL} && \
rm -f "${BACKUP_NAME}.sql.gz"
