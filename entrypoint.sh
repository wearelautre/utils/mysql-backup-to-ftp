#!/bin/bash
set -euxo pipefail

if [[ -n "${CRON_JOB}" ]]
then
    echo "${CRON_JOB} $(pwd)/backup.sh" > /var/spool/cron/crontabs/root
    crond -f
else
    echo "No CRON scheduling set"
    return 1
fi
