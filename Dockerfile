FROM alpine

RUN apk add --no-cache mysql-client curl

ADD entrypoint.sh backup.sh /app/

RUN chmod +x /app/*.sh
WORKDIR /app/

ENTRYPOINT ["sh", "/app/entrypoint.sh"]
CMD [""]
