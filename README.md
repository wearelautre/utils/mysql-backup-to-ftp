# mysql-backup

## Credits
This mysql backup image is based on [vmpartner](https://github.com/vmpartner/mysql-buckup) work.
I rework the cron job addition in order to put it in my swarm environment  

Automatic/periodic cron backup MySQL DB to FTP server by lightweight docker image with simple settings 🌂

## Description
This image making automatic backup from any mysql host to ftp server by mysqldump and curl utils.

## Usage
You can use mysql-backup by docker-compose or swarm:
```
version: '3'

services:
    mysql-backup:
        image: lautre/mysql-backup-to-ftp:latest
        environment:
        environment:
          MYSQL_HOST: mysql
          MYSQL_USER: my_db_user
          MYSQL_PASSWORD: my_db_password
          MYSQL_DATABASE: my_db_name
          FTP_USER: my_ftp_user
          FTP_PASSWORD: my_ftp_password
          FTP_URL: "ftp://ftp-server/path/to/folder"
          CRON_JOB: "0 */6 * * *"
        deploy:
          replicas: 1
          update_config:
            parallelism: 2
            delay: 30s
          restart_policy:
            condition: on-failure
            max_attempts: 3
            delay: 60s
```

or by docker run:
```
docker run \
    -dit \
    --name mysql-backup \
    -e MYSQL_HOST=mysql \
    -e MYSQL_USER=my_db_user \
    -e MYSQL_PASSWORD=my_db_password \
    -e MYSQL_DATABASE=my_db_name \
    -e FTP_USER=my_ftp_user \
    -e FTP_PASSWORD=my_ftp_password \
    -e FTP_URL="ftp://ftp-server/path/to/folder" \
    -e CRON_JOB="0 */6 * * *" \
    lautre/mysql-backup-to-ftp:latest
```

You need set MYSQL, FTP details and periodic task CRON_JOB in cron format. If you don't know cron, please read more at https://en.wikipedia.org/wiki/Cron

